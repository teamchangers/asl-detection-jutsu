class Features:
    """
    Class that is used as a container for the features extracted from images and
    used for the learning algorithms.
    """
    def __init__(self, label, peakNo, distances, ratios):
        """
        :param peakNo: number of peaks in histogram/picture
        :param distances: array with tuples (x[i], y[i]) where i <= 4
            x[i] = mod(x[i+1]-x[i]), if there are at least i peaks
            x[i] = 0, else
            y[i] = mod(y[i+1]-y[i]), if there are at least i peaks
            y[i] = 0, else
        :param ratios: array with tuples (rx[i], ry[i]) where i <= 4
            rx[i] = x[i]/x[i+1], where  x[i+1] != 0
            ry[i] = y[i]/y[i+1], where y[i+1] != 0
        """
        self.label = label
        self.peakNo = peakNo
        self.distances = distances
        self.ratios = ratios