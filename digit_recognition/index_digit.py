# USAGE
# python index.py --sprites sprites --index index.cpickle

# import the necessary packages
from digit_recognition.features import Features
import numpy as np
import argparse
import _pickle
import glob
import cv2
import math
import matplotlib.pyplot as plt
import re

# the list of features
features_list = []

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--sprites", required=True,
                help="Path where the sprites will be stored")
ap.add_argument("-i", "--features", required = True,
	help = "Path to where the feature file will be stored")
args = vars(ap.parse_args())



def show_image(title, img):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_interest_rectangle(img, bounding_rectangle_x1, bounding_rectangle_y1, bounding_rectangle_x2, bounding_rectangle_y2,
                           interest_rectangle_width,
                           interest_rectangle_height):
    """
    Draws the interest rectangle, which is positioned in the area with most non-black pixels of the image. (the palm)
    This is done because we need to identify the area of the palm, in order to remove it from the image.
    Parameters:
         the image
         the bounding rectangle's upper left corner, width and height
         the interest rectangle's width and height
    """
    interest_rectangle_x1 = bounding_rectangle_x1
    interest_rectangle_y1 = bounding_rectangle_y1
    non_black_pixels_max = 0
    for x in range(bounding_rectangle_x1, bounding_rectangle_x2, 10):
        for y in range(bounding_rectangle_y1, bounding_rectangle_y2, 10):
            roi = img[y:(y+interest_rectangle_height), x:(x+interest_rectangle_width)]
            # show_image("Region Of Interest", roi)
            non_black_pixels = cv2.countNonZero(roi)
            if non_black_pixels > non_black_pixels_max:
                non_black_pixels_max = non_black_pixels
                interest_rectangle_x1 = x
                interest_rectangle_y1 = y

    interest_rectangle_x2 = interest_rectangle_x1 + interest_rectangle_width
    interest_rectangle_y2 = interest_rectangle_y1 + interest_rectangle_height
    roi = img[interest_rectangle_y1:interest_rectangle_y2, interest_rectangle_x1:interest_rectangle_x2]
    # show_image('Palm ROI', roi)
    return interest_rectangle_x1, interest_rectangle_y1, interest_rectangle_x2, interest_rectangle_y2

def get_histogram_white_pixels_by_column(img):
    """
    For each coordinate x in the image, count the number of white pixels. (x, .)
    """
    img_width = img.shape[1]
    histogram = np.zeros(img_width)
    for row in img:
        for index in range(0, img_width):
            if row[index] != 0:
                histogram[index] += 1
    return histogram

def get_peaks(hist):
    """
    Gets the peaks from a 1D histogram.
    When a peak is found, the next 10 pixels on the x axis are skippe as they can be on the same level and
    interfere with the expected result.
    Also some stubs are removed by only getting the peaks that are over 25px in height.
    """
    peaks = []
    len_hist = len(hist)
    for i in range(len_hist):
        if i == 0:
            if hist[i] > hist[i+1]:
                if hist[i] > 25: # we eliminate the stubs
                    peaks.append((i, hist[i][0]))
                    i += 10
        elif i == len_hist - 1:
            if hist[i] > hist[i-1]:
                if hist[i] > 25: # we eliminate the stubs
                    peaks.append((i, hist[i][0]))
                    i += 10
        elif hist[i-1] < hist[i] and hist[i] >= hist[i+1]:
            if hist[i] > 25:  # we eliminate the stubs
                peaks.append((i, hist[i][0]))
                i += 10
    return peaks

def get_distances(peakNo, peaks, whichOne):
    """
    :param peakNo: number of peaks
    :param peaks: a tuple array of the following format
        [(x1, y1), (x2, y2),...]
        where y1 is the amplitude at position x1
    :param whichOne: distances/ratios
    :return: an array of values
    """
    distances = []
    for i in range(peakNo - 1):
        distances.append(abs(peaks[i + 1][whichOne] - peaks[i][whichOne]))
    for i in range(peakNo, 5):
        distances.append(0)
    return distances

def get_features(spritePath):
    # Get the digit shown by the hand in the picture
    digit = re.compile("/hand._").split(spritePath)[1][0]
    # Read the image
    img = cv2.imread(spritePath)

    # pad the image with extra black pixels
    img = cv2.copyMakeBorder(img, 30, 0, 30, 30, cv2.BORDER_CONSTANT, value=0)
    # invert the image and threshold it
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    value = (35, 35)
    blurred = cv2.GaussianBlur(grey, value, 0)
    _, thresh = cv2.threshold(blurred, 127, 255,
                              cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    thresh = cv2.bitwise_not(thresh)
    # Copy the original, threshholded image
    original = np.copy(thresh)

    (version, _, _) = cv2.__version__.split('.')

    if version is '3':
        _, contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    elif version is '2':
        contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    cnt = max(contours, key=lambda x: cv2.contourArea(x))

    bounding_rectangle_x1, bounding_rectangle_y1, bounding_rectangle_width, bounding_rectangle_height = cv2.boundingRect(
        cnt)

    # using real anatomical proportions
    interest_rectangle_width = int(0.44 * bounding_rectangle_height)
    interest_rectangle_height = int(0.496 * bounding_rectangle_height)

    bounding_rectangle_x2 = bounding_rectangle_x1 + bounding_rectangle_width
    bounding_rectangle_y2 = bounding_rectangle_y1 + bounding_rectangle_height
    # draw bounding rectangle [RED]
    cv2.rectangle(img, (bounding_rectangle_x1, bounding_rectangle_y1),
                  (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 255), 0)
    # draw interest rectangle [BLUE]
    interest_rectangle_x1, interest_rectangle_y1, interest_rectangle_x2, interest_rectangle_y2 = get_interest_rectangle(
        thresh, bounding_rectangle_x1, bounding_rectangle_y1, bounding_rectangle_width, bounding_rectangle_height,
        interest_rectangle_width, interest_rectangle_height)
    cv2.rectangle(img, (interest_rectangle_x1, interest_rectangle_y1),
                  (interest_rectangle_x2, interest_rectangle_y2), (255, 0, 0), 0)
    # remove all skin pixels under interest rectangle
    cv2.rectangle(img, (bounding_rectangle_x1, interest_rectangle_y2),
                  (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 0), -1)
    cv2.rectangle(original, (bounding_rectangle_x1, interest_rectangle_y2),
                  (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 0), -1)
    # draw ellipse
    center_x = int((interest_rectangle_x1 + interest_rectangle_x2) / 2)
    center_y = int((interest_rectangle_y1 + interest_rectangle_y2) / 2)
    cv2.ellipse(img, (center_x, center_y),
                (int(interest_rectangle_width / 2 ** 0.5), int(interest_rectangle_height / 2 ** 0.5)), 0, 0, 360,
                (0, 0, 0), -1)
    cv2.ellipse(original, (center_x, center_y),
                (int(interest_rectangle_width / 2 ** 0.5), int(interest_rectangle_height / 2 ** 0.5)), 0, 0, 360,
                (0, 0, 0), -1)

    hull = cv2.convexHull(cnt)
    drawing = np.zeros(img.shape, np.uint8)
    cv2.drawContours(drawing, [cnt], 0, (0, 255, 0), 0)
    cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 0)
    hull = cv2.convexHull(cnt, returnPoints=False)

    # get histogram
    hist = get_histogram_white_pixels_by_column(original)

    # apply gaussian filter
    hist = cv2.GaussianBlur(hist, (11, 83), 0,
                            11)  # arbitrary parameters, they can be changed but it's ok the way they are now

    peaks = get_peaks(hist)
    print("THE DIGIT:\n" + digit)
    number_of_peaks = len(peaks)
    print("THE " + str(number_of_peaks) + " PEAKS:\n" + str(peaks))

    # get the distances
    distances = get_distances(number_of_peaks, peaks, 0)
    # get the ratios
    ratios = get_distances(number_of_peaks, peaks, 1)
    # create Features object
    features = Features(digit, number_of_peaks, distances, ratios)
    # if number_of_peaks > 5:
    #     print(spritePath)
    return features

def just_run():
    # loop over the sprite images
    for spritePath in glob.glob(args["sprites"] + "/*.png"):
        features_list.append(get_features(spritePath))

def present():
    # loop over the sprite images
    for spritePath in glob.glob(args["sprites"] + "/*.png"):
        # Get the digit shown by the hand in the picture
        digit = re.compile("/hand._").split(spritePath)[1][0]
        # Read the image
        img = cv2.imread(spritePath)

        # pad the image with extra black pixels
        img = cv2.copyMakeBorder(img, 30, 0, 30, 30, cv2.BORDER_CONSTANT, value=0)
        # invert the image and threshold it
        grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        value = (35, 35)
        blurred = cv2.GaussianBlur(grey, value, 0)
        _, thresh1 = cv2.threshold(blurred, 127, 255,
                                   cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        thresh1 = cv2.bitwise_not(thresh1)
        # Copy the original, threshholded image
        original = np.copy(thresh1)
        show_image("thresholded", thresh1)

        (version, _, _) = cv2.__version__.split('.')

        if version is '3':
            _, contours, hierarchy = cv2.findContours(thresh1.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        elif version is '2':
            contours, hierarchy = cv2.findContours(thresh1.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        cnt = max(contours, key=lambda x: cv2.contourArea(x))

        bounding_rectangle_x1, bounding_rectangle_y1, bounding_rectangle_width, bounding_rectangle_height = cv2.boundingRect(
            cnt)

        # using real anatomical proportions
        interest_rectangle_width = int(0.44 * bounding_rectangle_height)
        interest_rectangle_height = int(0.496 * bounding_rectangle_height)

        bounding_rectangle_x2 = bounding_rectangle_x1 + bounding_rectangle_width
        bounding_rectangle_y2 = bounding_rectangle_y1 + bounding_rectangle_height
        # draw bounding rectangle [RED]
        cv2.rectangle(img, (bounding_rectangle_x1, bounding_rectangle_y1),
                      (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 255), 0)
        # draw interest rectangle [BLUE]
        interest_rectangle_x1, interest_rectangle_y1, interest_rectangle_x2, interest_rectangle_y2 = get_interest_rectangle(
            thresh1, bounding_rectangle_x1, bounding_rectangle_y1, bounding_rectangle_width, bounding_rectangle_height,
            interest_rectangle_width, interest_rectangle_height)
        cv2.rectangle(img, (interest_rectangle_x1, interest_rectangle_y1),
                      (interest_rectangle_x2, interest_rectangle_y2), (255, 0, 0), 0)
        # remove all skin pixels under interest rectangle
        cv2.rectangle(img, (bounding_rectangle_x1, interest_rectangle_y2),
                      (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 0), -1)
        cv2.rectangle(original, (bounding_rectangle_x1, interest_rectangle_y2),
                      (bounding_rectangle_x2, bounding_rectangle_y2), (0, 0, 0), -1)
        # show_image('AFTER RECTANGLES', img)

        # draw ellipse
        center_x = int((interest_rectangle_x1 + interest_rectangle_x2) / 2)
        center_y = int((interest_rectangle_y1 + interest_rectangle_y2) / 2)
        cv2.ellipse(img, (center_x, center_y),
                    (int(interest_rectangle_width / 2 ** 0.5), int(interest_rectangle_height / 2 ** 0.5)), 0, 0, 360,
                    (0, 0, 0), -1)
        cv2.ellipse(original, (center_x, center_y),
                    (int(interest_rectangle_width / 2 ** 0.5), int(interest_rectangle_height / 2 ** 0.5)), 0, 0, 360,
                    (0, 0, 0), -1)
        show_image('Ellipse', img)

        hull = cv2.convexHull(cnt)
        drawing = np.zeros(img.shape, np.uint8)
        cv2.drawContours(drawing, [cnt], 0, (0, 255, 0), 0)
        show_image("contour", drawing)
        cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 0)
        show_image("hull+contour", drawing)
        hull = cv2.convexHull(cnt, returnPoints=False)
        cv2.drawContours(thresh1, contours, 0, (0, 255, 0), 0)
        show_image("hull+contour2", drawing)
        show_image("threshholded2", thresh1)

        show_image('INTEREST', img)
        all_img = np.hstack((drawing, img))
        show_image('Result', all_img)
        show_image('Final result', original)

        # get histogram
        hist = get_histogram_white_pixels_by_column(original)
        plt.plot(hist)

        # apply gaussian filter
        hist = cv2.GaussianBlur(hist, (11, 83), 0,
                                11)  # arbitrary parameters, they can be changed but it's ok the way they are now
        plt.plot(hist)
        plt.title(digit + " BEFORE+AFTER GAUSSIAN BLUR")
        plt.show()

        peaks = get_peaks(hist)
        print("THE DIGIT:\n" + digit)
        number_of_peaks = len(peaks)
        print("THE " + str(number_of_peaks) + " PEAKS:\n" + str(peaks))

        # get the distances
        distances = get_distances(number_of_peaks, peaks, 0)
        # get the ratios
        ratios = get_distances(number_of_peaks, peaks, 1)
        # create Features object
        features = Features(digit,number_of_peaks, distances, ratios)
        features_list.append(features)

# present()
just_run()
# write the features to file
with open(args["features"], 'wb') as file:
    _pickle.dump(features_list, file)