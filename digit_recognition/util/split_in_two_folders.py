import argparse
import os
import glob
from random import random

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("--o", required=True,
                help="The original file location")
ap.add_argument("--train", required=True,
                help="Path for train files(70%)")
ap.add_argument("--test", required = True,
	help = "Path for test files(30%)")
args = vars(ap.parse_args())

for spritePath in glob.glob(args["o"] + "/*.png"):
    list = spritePath.split("data/digits/")
    if random() <= 0.7:
        os.rename(spritePath, args["train"] + "/" + list[1])
    else:
        os.rename(spritePath, args["test"] + "/" + list[1])