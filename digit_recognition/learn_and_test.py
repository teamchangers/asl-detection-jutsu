from sklearn import tree
import _pickle
import glob

def load_cpickle(file):
    with open(file, 'rb') as file:
        return _pickle.load(file)

#read trainingfeatures
training_features_list = load_cpickle("training_features.cpickle")
#read testingfeatures
testing_features_list = load_cpickle("testing_features.cpickle")

# LEARN / TRAIN
X = []
Y = []
mai_mari_ca_5 = 0
for feature in training_features_list:
    if(feature.peakNo > 5):
        mai_mari_ca_5 += 1
        continue
    X.append([feature.peakNo, *feature.distances, *feature.ratios])
    Y.append(feature.label)
clf = tree.DecisionTreeClassifier()

print("MAIMARICA5TRAINING: " + str(mai_mari_ca_5))
clf = clf.fit(X, Y)


# TEST
total = [0] * 10
correct = [0] * 10
to_predict = []
expected_values = []
mai_mari_ca_5_test = 0
for feature in testing_features_list:
    if (feature.peakNo > 5):
        mai_mari_ca_5_test += 1
        continue
    to_predict.append([feature.peakNo, *feature.distances, *feature.ratios])
    expected_values.append(feature.label)
print("MAIMARICA5TEST:", mai_mari_ca_5_test)
result = clf.predict(to_predict)

corecte = 0
corectepecifre = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0,'6':0, '7':0, '8':0,'9':0}
toatepecifre = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0,'6':0, '7':0, '8':0,'9':0}
rezultatpecifre = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0,'6':0, '7':0, '8':0,'9':0}

for i in range(len(expected_values)):
    if expected_values[i] == result[i]:
        corectepecifre[expected_values[i]] += 1
        corecte += 1
    toatepecifre[expected_values[i]] += 1
print(corectepecifre)
print(toatepecifre)

for k in toatepecifre.keys():
    rezultatpecifre[k] = corectepecifre[k] * 100 / toatepecifre[k]
print("Procente pe cifre: ", rezultatpecifre)
print("Result: " + str(corecte) + "/" + str(len(expected_values)) + "(" +str((corecte*100)/len(expected_values)) +"%)")

