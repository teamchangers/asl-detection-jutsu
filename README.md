#Traducerea limbajului semnelor(ASL) in text
#Autori: Costin Bianca Ioana, Idomir Razvan


##Descrierea proiectului
Scopul acestui proiect e identificarea semnelor folosite de nevorbitori in comunicare, avand ca date de intrare o imagine care contine unul dintre semnele folosite. Sistemul va putea categoriza imaginea intr-una din cele 26 litere din alfabetul englez.
Nu face parte din scopul proiectului identificarea semnelor care implica miscari ale mainii sau utilizarea ambelor brate, asa ca am ales doar semnele statice, care pot fi cuprinse in imagini. In functie de rezultatele obtinute, proiectul poate fi extins sa cuprinda o baza mai mare de semne si sa poata fi folosit la traducerea limbajului dinamic al semnelor folosite de nevorbitori in text.


##Obiective
* Detectarea mainii intr-o imagine clara, cu fundal uniform.
* Detectarea punctelor de interes (palma, varfurile degetelor)
* Selectarea caracteristicilor importante pentru algoritmul de invatare (inclinatia degetelor fata de incheietura, orientarea lor spre exterior, grosimea palmei detectate raportata la lungimea degetelor etc)
* Selectarea datelor de antrenament si test.
* Incercarea a 2 algoritmi de invatare.
* Evaluarea performantei algoritmilor.
* Detectarea mainii intr-o imagine avand fundal neuniform.

##Formatul datelor de test
Datele initiale de test vor fi imagini clare ale mainii pe un fundal alb, iar dupa gasirea unui algoritm suficient de bun pentru etichetarea acestora dorim sa incercam sa rezolvam problema in cazul introducerii de zgomot (eg. orice fel de fundal).

EX: 

* http://sun.aei.polsl.pl/~mkawulok/gestures/
* http://www.massey.ac.nz/~albarcza/gesture_dataset2012.html


##Documentatie
* [Real-time American Sign Language Recognition with Convolutional Neural Networks](  http://cs231n.stanford.edu/reports2016/214_Report.pdf)


* [Converting sign language gestures from digital images to text using Fourier Descriptors](https://www.youtube.com/watch?v=bG0k3zQSa7w)
 * [Hand tracking and gesture recognition system for human-computer interaction using low-cost hardware](http://link.springer.com/article/10.1007/s11042-013-1501-1)(http://sci-hub.io/10.1007/s11042-013-1501-1)
* [Improving the learning speed of 2-layer Neural Networks by choosing initial values of the adaptive weights](https://web.stanford.edu/class/ee373b/nninitialization.pdf)


* [Feature extraction](http://inpressco.com/wp-content/uploads/2012/12/Paper1323-3272.pdf)

* [Install OpenCV 3.0](http://www.pyimagesearch.com/2015/07/20/install-opencv-3-0-and-python-3-4-on-ubuntu/)